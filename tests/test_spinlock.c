

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>
#include "spinlock.h"

int i = 0;
spinlock_t lock;

void thread1()
{
    for (;;) {
        spinlock_lock(&lock);
        assert(i+1==++i);
        spinlock_unlock(&lock);
    }
}

void thread2()
{
    for (;;) {
        spinlock_lock(&lock);
        assert(i+1==++i);
        spinlock_unlock(&lock);
    }
}

int main(void)
{
    pthread_t ptid1, ptid2;
    
    spinlock_init(&lock);

    pthread_create(&ptid1, NULL, thread1, NULL);
    pthread_create(&ptid2, NULL, thread2, NULL);

    pthread_join(ptid1, NULL);
    pthread_join(ptid2, NULL);
    
    spinlock_destory(&lock);
    return 0;
}
