

#ifndef __SPINLOCK__H__
#define __SPINLOCK__H__


struct spinlock_s {
    int lock;
};

typedef struct spinlock_s spinlock_t;


static inline void 
spinlock_init(spinlock_t *lock)
{
    lock->lock = 0;
}


static inline void
spinlock_lock(spinlock_t *lock) 
{
    while (__sync_lock_test_and_set(&lock->lock, 1));
}


static inline void
spinlock_trylock(spinlock_t *lock)
{
    return __sync_lock_test_and_set(&lock->lock, 1) == 0;
}


static inline void
spinlock_unlock(spinlock_t *lock)
{
    __sync_lock_release(&lock->lock);
}


static inline void
spinlock_destory(spinlock_t *lock)
{
    (void) lock;
}


#endif

